/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package database.component;

import com.werapan.databaseproject.model.Product;

/**
 *
 * @author buy68
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
